import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { Storage } from '@ionic/storage';
import { VideoPlayer } from '@ionic-native/video-player';
import {ShownotificationPage} from '../shownotification/shownotification';


@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

 data : any;
not : any;
notifications : any;
historyShow:true;
notificationData : any;
bgColorForRead : string= "blue";
bgColorForunRead : string= "lightgrey";
canLogin : boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  	public storage:Storage,public user:UserserviceProvider,public loadingCtrl: LoadingController,private videoPlayer: VideoPlayer,
  ) {

    this.notifications=this.navParams.get('notifications');
    console.log('constuctor');

    this.storage.get('can_login').then((val) => {
          
          console.log('login',val);
          if(val==true){
            this.canLogin=true;
          }
          else{

            this.canLogin=false;
          }

        });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationsPage');
  }

  ionViewDidEnter(){
    console.log('didload');
    this.loadData();

  }

  loadData(){
    
    let loader = this.loadingCtrl.create({
        content : "Please wait..."
    });

    loader.present();
    this.storage.get('api_token').then((val)=>{
      
      loader.dismiss();
        this.user.getNotificationData(val).then(data2=>{


            this.notificationData=data2['data'];
            console.log('lenght',this.notificationData);

       //     console.log('count',this.notification_count);
    
            this.notifications=this.notificationData['notifications'];

            console.log('lenght',this.notifications);
            /*if(this.notifications.lenght<0){
              // this.historyShow=false;
            }
            else{
              this.notifications=true;
            }*/
           
            console.log(this.notificationData);
          }).catch(err=>{      

            console.log(err);
        });
    });
  }
 
 	openVideo(notifyObject){
console.log('notification',notifyObject);
  this.navCtrl.push(ShownotificationPage,{notification:notifyObject});

}

}
