import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AlertController,ModalController,ToastController } from 'ionic-angular';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import {ModalpagePage} from '../modalpage/modalpage';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-redeemcode',
  templateUrl: 'redeemcode.html',
})
export class RedeemcodePage {

  modalData : any;
  isScanned: boolean;
  isScannedInput = false;
  scannedCodes=[];
  i=0;k=0;l=0;
  flag=0;
  reedemCodeDigits = 0;
  isDealer : boolean =false;
  redeemcode='';
  codeAddition=[];

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl : ModalController,
  			  private barcodeScanner: BarcodeScanner,public user:UserserviceProvider,public alertCtrl: AlertController,
          public storage:Storage,public toastCtrl:ToastController) {
    
  	this.isScanned=false;

this.storage.get("role").then((val)=>{
    console.log("role",val);
    if(val=="dealer"){
      this.isDealer=true;
      this.reedemCodeDigits =15;
    }
    else{
      this.isDealer=false;
      this.reedemCodeDigits =16;
    }
  });

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad RedeemcodePage');
  }


  scan(){
	  this.flag=0;
	  this.barcodeScanner.scan().then(barcodeData => {
  		if(barcodeData.text != ''){
	 		if(this.i>0){
				this.isScanned=true;
				for(var j=0;j<= this.i;j++){
  				if(this.scannedCodes[j] == barcodeData.text){
	   				this.flag += 1; 
		  			alert("u already scanned this code");
			   	}
        }
        if(this.flag==0){
				  this.scannedCodes[this.i]=barcodeData.text;
				  this.i += 1;
        }
			}
			else{
				this.scannedCodes[this.i]=barcodeData.text;
				this.i += 1;
				this.isScanned=true;
			}
		}
		 this.redeemcode=this.scannedCodes.toString();
		}).catch(err => {
		    console.log('Error', err);
		});
		this.isScannedInput = true;
  }
  addCode(){
  	this.codeAddition=[];
  	this.codeAddition=this.redeemcode.split(',');

  	this.scannedCodes=[];
  	for(this.k=0;this.k<this.codeAddition.length;this.k++){
  		if(this.codeAddition[this.k]==''){
  			for(this.l=this.k+1;this.l<this.codeAddition.length;this.l++){
  				this.codeAddition[this.k]=this.codeAddition[this.l];
  			}
  		}  		
  	}
  	for(this.k=0;this.k<this.codeAddition.length;this.k++){
      if(this.codeAddition[this.k]!=''){
  		this.scannedCodes[this.k]=this.codeAddition[this.k];
    }
  	}
  	this.i=this.scannedCodes.length;
    if(this.codeAddition.length==1){
      if(this.codeAddition[0]==''){
        this.i=0;
      }
    }
  	if(this.i>0){
  	  this.isScanned=true;
  	}if(this.i==0){
  	  this.isScanned=false;
  	}
  }
  submitCode(){
    this.storage.get('api_token').then(token=>{
        this.storage.get('mobile_no').then(mobile=>{
            this.user.redeemCoupon(token,mobile,this.scannedCodes).then(res=>{
              console.log(res);/*
              let toast=this.toastCtrl.create({
                  message: res['message'],
                  duration: 3000,
                  position: 'top'
              });
              toast.present();*/
              this.scannedCodes=[];
              this.redeemcode='';
              this.i=0;
              this.codeAddition=[];
              this.flag=0;
              this.isScanned=false;              
              this.presentAlert(res['message']);
            });
        });
    });
  	
  }
  presentAlert(message) {
    let alert = this.alertCtrl.create({
      
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.setRoot(this.navCtrl.getActive().component);
            console.log('Ok clicked');
          }
        }
      ]
    });
    alert.present();
  }

}
