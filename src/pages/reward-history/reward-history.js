var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RewardProvider } from '../../providers/reward/reward';
import { AlertController, ModalController } from 'ionic-angular';
import { ModalpagePage } from '../modalpage/modalpage';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
var RewardHistoryPage = /** @class */ (function () {
    function RewardHistoryPage(navCtrl, navParams, rewardapi, alertCtrl, modalCtrl, storage, datepipe) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.rewardapi = rewardapi;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.datepipe = datepipe;
        this.getRewardHistory();
    }
    RewardHistoryPage_1 = RewardHistoryPage;
    RewardHistoryPage.prototype.getRewardHistory = function () {
        var _this = this;
        var date = new Date();
        var fromdate = this.datepipe.transform(date, 'yyyy-MM-dd');
        var todate = this.datepipe.transform(date, 'yyyy-MM-dd');
        if (this.modalData != undefined) {
            fromdate = this.modalData['fromDate'];
            todate = this.modalData['toDate'];
            if (fromdate == undefined || todate == undefined) {
                fromdate = '';
                todate = '';
            }
        }
        else {
            fromdate = '';
            todate = '';
        }
        this.storage.get('api_token').then(function (token) {
            _this.rewardapi.getRewardHistoryList(token, fromdate, todate).then(function (data2) {
                console.log(data2);
                _this.dataSet = data2['data'];
            });
        });
    };
    RewardHistoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RewardHistoryPage');
    };
    RewardHistoryPage.prototype.presentAlert = function () {
        var _this = this;
        var modal = this.modalCtrl.create(ModalpagePage, { page: RewardHistoryPage_1 });
        modal.present();
        modal.onDidDismiss(function (data) {
            _this.modalData = data;
            if (_this.modalData != undefined) {
                if (_this.modalData.fromDate != '' && _this.modalData.toDate != '') {
                    console.log(_this.modalData.fromDate);
                    console.log(_this.modalData.toDate);
                    console.log(data);
                }
            }
            _this.getRewardHistory();
        });
    };
    var RewardHistoryPage_1;
    RewardHistoryPage = RewardHistoryPage_1 = __decorate([
        IonicPage(),
        Component({
            selector: 'page-reward-history',
            templateUrl: 'reward-history.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams,
            RewardProvider, AlertController,
            ModalController, Storage, DatePipe])
    ], RewardHistoryPage);
    return RewardHistoryPage;
}());
export { RewardHistoryPage };
//# sourceMappingURL=reward-history.js.map