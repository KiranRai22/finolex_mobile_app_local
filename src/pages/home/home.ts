import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  constructor(public navCtrl: NavController,public alertCtrl: AlertController) {

  }

presentAlert(){

 let alert = this.alertCtrl.create({
    title: 'Dates',
    subTitle: '10% of battery remaining',
    inputs: [
        {
          name: 'from Date',
          placeholder: 'From Date',
          type: 'date',
          id: 'from_date'
        },
        {
        name: 'To date',
          placeholder: 'To Date',
          type: 'date'	
        }
      ],
    buttons: ['Dismiss']
  });
  alert.present();
  }

}
