import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-products-modal',
  templateUrl: 'products-modal.html',
})
export class ProductsModalPage {

 data : any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

  	console.log(this.navParams.get('productData'));

    this.data=this.navParams.get('productData');

  	console.log('received',this.data);

  }

  ionViewDidLoad() {

  }

}
