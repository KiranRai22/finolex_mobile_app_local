import { Component } from '@angular/core';
import { NavController, NavParams,ModalController } from 'ionic-angular';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { AlertController,LoadingController } from 'ionic-angular';
import * as HighCharts from 'highcharts';
import { Storage } from '@ionic/storage';
import {ModalpagePage} from '../modalpage/modalpage';
import { DatePipe } from '@angular/common';
import { Events } from 'ionic-angular';
import {NotificationsPage} from '../notifications/notifications';
import { TranslateService } from '@ngx-translate/core';
import { AppVersion } from '@ionic-native/app-version';
import { Market } from '@ionic-native/market';

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

	totalPoints;
	totalValue;
	todayPoints;
	thisWeekPoints;
  redeemedValue:any;
  thisMonthPoints:any;
	token;
  notification_count = 0;
  notifications : any; 
  notificationData : any;
  todate : any;
  weekStartDay : any;
  weekStartDayDisp : any;
  modalData : any;
  dashboardChart:any;label:any;
  showChart:any;
  show:boolean;
  messege;
  weekEndDay : any;
  weekEndDayDisp: any;
  unreadNotifictaions : boolean=true; 
  canLogin : boolean =true;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  	public user:UserserviceProvider,private storage: Storage,
    public loadingCtrl: LoadingController,
    public translateService: TranslateService,
    private appVersion : AppVersion,
    private market : Market,
    public alertCtrl: AlertController,public modalCtrl : ModalController,
    public datepipe: DatePipe,public events: Events) {
    this.events.publish('user:created', 'user', Date.now());


     this.storage.get('can_login').then((val) => {
          
          console.log('login',val);
          if(val==true){
            this.canLogin=true;
          }
          else{

            this.canLogin=false;
          }

        });

   //  this.checkAppVersion();

    this.weekStartDay=new Date();
    this.weekEndDay=this.weekStartDay;

    this.weekEndDay = new Date();
    this.weekEndDay=this.datepipe.transform(this.weekEndDay, 'yyyy-MM-dd');
    this.weekEndDayDisp = this.datepipe.transform(this.weekEndDay, 'dd-MM-yyyy');

    this.getLastSunday(this.weekStartDay);
       
		this.dashboardChart=[];
    this.showChart=0;
    
 	//	this.statistics();
    // this.weekStartDay=this.datepipe.transform(this.weekStartDay, 'yyyy-MM-dd');

  }

  ionViewDidLoad() {
   // this.checkAppVersion();
    this.statistics();
    console.log("view load");
  }
  renderchart(chartLoadData){

    var myChart = HighCharts.chart('BarChartContainer', {
      chart: {
        type: 'bar',
        //renderTo: 'BarChartContainer',
        plotBackgroundColor: 'transparent',
        backgroundColor: {
          linearGradient: [0, 0, 500, 500],
          stops: [
              [0, '#059de8'],
              [1, '#1f6fd3']
          ]
      },
        spacingBottom: 25,
        spacingTop: 25,
        spacingLeft: 25,
        spacingRight: 25
      },
      title: {
          text: ''
      },
      xAxis: {
        categories: this.label,
        labels: {
          style: {
              color: 'white'
          }
      }
      },
      yAxis: {
         gridLineColor: false,
         labels: {
         	enabled:false,
           style: {
               color: 'white'
           }
          },
          title: {
            text: ''
          }
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
              dataLabels: {
                  enabled: true,
                  align: 'right'
              }
          }
      },
      series: [{
        color: 'white',
        shadow: {
          color: 'white',
          width: 4,
          offsetX: 0,
          offsetY: 0
        },
       // var dash=[0, 0, 0, 0, 0, 0, 0];
        data: chartLoadData

      }]
    });
   

  }
  statistics(){
    var date=new Date();
    var fromdate=this.datepipe.transform(this.weekStartDay, 'yyyy-MM-dd');

  	this.storage.get('api_token').then((val) => {
  		this.token = val;
  	  		this.user.statisticsService(this.token,fromdate).then((data)=>{
  	  			var chartData=data['data'].connects_data;
            this.dashboardChart=[];
            this.showChart=0;
            this.label=['S','M', 'T', 'W','T', 'F', 'S'];
  	  			for(let value of <any>Object.values(chartData)){
              if(value==0){
                  this.showChart=this.showChart+1;
                  this.dashboardChart.push(value);
              }
              else{
                  this.dashboardChart.push(Number(value));
              }
  	  			}
            if(this.showChart==Object.keys(chartData).length ){
              this.messege = 'No Data Found';
            }else{
                this.renderchart(this.dashboardChart);
            }

  		  		this.totalPoints=data['data'].total_points;
  		  		this.totalValue=data['data'].total_value;
  		  		this.todayPoints=data['data'].today_points;
  		  		this.thisWeekPoints=data['data'].this_week_points;
            this.thisMonthPoints=data['data'].this_month_points;
            this.redeemedValue=data['data'].redeemed_value;

  	  		});
      
  	});

  	
  }

  ionViewDidEnter(){
   //  this.checkAppVersion();
    this.loadData();
    console.log("view Enter");
  }

  change(){

    console.log('hindi')
    this.translateService.use('hi');

  }
  

  showNotifications(){
     this.navCtrl.push(NotificationsPage,{notifications:this.notifications}); 
  }

  loadData(){
    let loader = this.loadingCtrl.create({
        content : "Please wait..."
    });

    loader.present();
    this.storage.get('api_token').then((val)=>{
      
      loader.dismiss();
        this.user.getNotificationData(val).then(data2=>{

            this.notificationData=data2['data'];
            this.notification_count=this.notificationData['unread_notifications_count'];
            if(this.notification_count==0){
             this.unreadNotifictaions=false;
            }
            else{
             this.unreadNotifictaions=true;
            }
    
            this.notifications=this.notificationData['notifications'];

            /*if(this.notifications.lenght<0){
              // this.historyShow=false;
            }
            else{
              this.notifications=true;
            }*/
           
          }).catch(err=>{      
            console.log(err);
        });
    });
  }

  checkAppVersion(){

    console.log('update');

   /* this.appVersion.getVersionNumber().then((s) => {
      console.log('ver',s);
      if(s!="0.0.1"){
        this.market.open("com.finolex.sales");
      }
      else{
        this.initializeApp();

      }
     });*/


    this.user.checkVersion().then((dataSet)=>{

         console.log('languagApi',dataSet);
         if(dataSet['status']==true){
           
         //  this.presentAlert(dataSet['message']);
           console.log('message',dataSet['message']);
           var version=dataSet['data'].android_version;

           console.log('version',version);
           console.log('app version',this.appVersion.getVersionNumber());

        /*   if(version!=this.appVersion.getVersionNumber()){
             this.presentAlert(dataSet['message'])
             console.log("true")
           }
           else{
             this.initializeApp();
           }*/

     this.appVersion.getVersionNumber().then((s) => {
      console.log('ver',s);
      if(s!=version){
    //    this.market.open("com.finolex.sales");
    this.GotoPlaystore();
      }
      else{
        this.statistics();

      }
     });

         }
       });

   // this.market.open("com.finolex.sales");

  }

  GotoPlaystore() {
    let alert = this.alertCtrl.create({
      
      subTitle: "Finolex New Update is Available.Please Update the New Version",
      buttons: [
        {
          text: 'Ok',
          handler: () => {
              this.market.open("com.finolex.sales");
            console.log('Ok clicked');
          }
        }
      ]
    });
    alert.present();
  } 

    
  presentAlert(){
    let modal = this.modalCtrl.create(ModalpagePage,{page:DashboardPage,pageName:"DashboardPage"});
    modal.present();
    modal.onDidDismiss(data => {
      this.modalData=data;
      if(this.modalData != undefined){
        if(this.modalData.fromDate!=''){
        }      
      }
      this.getLastSunday(this.modalData.fromDate);
    });
 }

  getLastSunday(d) {
    this.weekStartDay = new Date(d);
    this.weekStartDay.setDate(this.weekStartDay.getDate() - this.weekStartDay.getDay());
    let weekSDay = this.weekStartDay;
    this.weekStartDay=this.datepipe.transform(this.weekStartDay, 'yyyy-MM-dd');
    this.weekStartDayDisp = this.datepipe.transform(this.weekStartDay, 'dd-MM-yyyy');
    let toDate = weekSDay.setDate( weekSDay.getDate() + 6   );
    this.weekEndDay = this.datepipe.transform(toDate, 'yyyy-MM-dd');
    this.weekEndDayDisp = this.datepipe.transform(this.weekEndDay, 'dd-MM-yyyy');
    this.statistics();
}

}
