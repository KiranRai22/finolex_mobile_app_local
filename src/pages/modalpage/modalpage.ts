import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController,ViewController,AlertController} from 'ionic-angular';


@Component({
  selector: 'page-modalpage',
  templateUrl: 'modalpage.html',
})

export class ModalpagePage {

page : any;
fromdate : any;
todate : any;
data : any;
datecheck:boolean = true;
pageName:any;
today : any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewctrl : ViewController,public alertCtrl: AlertController) {
  	this.page = navParams.get('page');
    this.pageName=navParams.get('pageName');

    this.today = new Date().toJSON().split('T')[0];


    if(this.pageName == "DashboardPage")
      this.datecheck = false;
  	console.log('data',this.pageName);

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalpagePage');
  }

  dismiss(){
this.viewctrl.dismiss();
  }

 getLastSunday(d) {
  var t = new Date(d);
  t.setDate(t.getDate() - t.getDay());
  console.log("dates",t);
  return t;
}

presentAlert(){
  var dates = [];
  console.log("vatakayi")
  console.log('page',this.page);
      dates.push({
          name: 'from Date',
          placeholder: 'From Date',
          type: 'date',
          id: 'from_date'
        });

      // if(this.page != "dashboard")
      //   dates.push({
      //   name: 'To date',
      //     placeholder: 'To Date',
      //     type: 'date'});
 let alert = this.alertCtrl.create({
    title: 'Dates',
    subTitle: '10% of battery remaining',
    inputs: dates,
    buttons: ['Dismiss']
  });
  alert.present();
  }

  submit(){
   
      this.data={fromDate : this.fromdate,toDate : this.todate};
      this.viewctrl.dismiss(this.data);
  }

}
