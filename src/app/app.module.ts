import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

// Pages in the Application
import { DashboardPage } from '../pages/dashboard/dashboard';
import { WelcomePage } from '../pages/welcome/welcome';
import { RedeemcodePage } from '../pages/redeemcode/redeemcode';
import { ChangeRewardPage } from '../pages/change-reward/change-reward';
import { RedeemHistoryPage } from '../pages/redeem-history/redeem-history';
import { RewardHistoryPage } from '../pages/reward-history/reward-history';
import { ProfilePage } from '../pages/profile/profile';
import { LogoutPage } from '../pages/logout/logout';
import { StatusBar } from '@ionic-native/status-bar';
import {ModalpagePage} from '../pages/modalpage/modalpage'
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserserviceProvider } from '../providers/userservice/userservice';
import { DashboardProvider } from '../providers/dashboard/dashboard';
import { RewardProvider } from '../providers/reward/reward';
import { RedeemProvider } from '../providers/redeem/redeem';
import { UpdateimageProvider } from '../providers/updateimage/updateimage';
import { IndstatesProvider } from '../providers/indstates/indstates';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpModule} from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { ReedemapiProvider } from '../providers/reedemapi/reedemapi';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import {ChangePasswordPage} from '../pages/change-password/change-password';
import {ProductsPage} from  '../pages/products/products';
import {TabsScreenPage} from '../pages/tabs-screen/tabs-screen';
import { ForgotPasswordPage} from '../pages/forgot-password/forgot-password';
import { ForgotpasswordProvider } from '../providers/forgotpassword/forgotpassword';
import {ConfirmpasswordPage} from '../pages/confirmpassword/confirmpassword';
import { SetpasswordProvider } from '../providers/setpassword/setpassword';
import { CheckotpProvider } from '../providers/checkotp/checkotp';
import {OtpPage} from '../pages/otp/otp';
import { Firebase  } from '@ionic-native/firebase';
import { DatePipe } from '@angular/common';
import {ProductsModalPage} from '../pages/products-modal/products-modal';
import { VideoPlayer } from '@ionic-native/video-player';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {NotificationsPage} from '../pages/notifications/notifications';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';
import {ShownotificationPage} from '../pages/shownotification/shownotification';
import {RedeemlistModalPage} from '../pages/redeemlist-modal/redeemlist-modal';
import { Badge } from '@ionic-native/badge';
import {ContactUsPage} from '../pages/contact-us/contact-us';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {Http } from '@angular/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AppVersion } from '@ionic-native/app-version';
import { Keyboard } from '@ionic-native/keyboard';
import { Market } from '@ionic-native/market';
import {ForceupdatePage} from '../pages/forceupdate/forceupdate';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DashboardPage,
    RedeemcodePage,
    ChangeRewardPage,
    RedeemHistoryPage,
    RewardHistoryPage,
    ProfilePage,
    LogoutPage,
    ForceupdatePage,
    ProductsPage,
    ModalpagePage,
    NotificationsPage,
    ChangePasswordPage,TabsScreenPage,
    OtpPage,
    ContactUsPage,
    RedeemlistModalPage,
    ShownotificationPage,
    ConfirmpasswordPage,ForgotPasswordPage,ProductsModalPage
    ,WelcomePage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,HttpModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DashboardPage,
    RedeemcodePage,
    ChangeRewardPage,
    RedeemHistoryPage,
    RewardHistoryPage,
    ProfilePage,
    LogoutPage,
    ForceupdatePage,
    ProductsPage,
    ContactUsPage,
    NotificationsPage,
    ModalpagePage,
    RedeemlistModalPage,
    ShownotificationPage,
    ChangePasswordPage,TabsScreenPage,ForgotPasswordPage,
    OtpPage,ConfirmpasswordPage,ProductsModalPage
    ,WelcomePage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserserviceProvider,
    DashboardProvider,
    RewardProvider,
    RedeemProvider,
    UpdateimageProvider,
    IndstatesProvider,Camera,BarcodeScanner,HttpModule,
    RewardProvider,
    ScreenOrientation,
    ReedemapiProvider,
    ForgotpasswordProvider,
    SetpasswordProvider,
    CheckotpProvider,Firebase,DatePipe,
    InAppBrowser,
    VideoPlayer,
    AppVersion,
    StreamingMedia,
    Badge,Keyboard,
    Market
   
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class AppModule {}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}