import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Http,Headers} from '@angular/http';

@Injectable()
export class UserserviceProvider {


  host: string = 'http://app.meltag.com/finolex/api/';
  url:string='http://app.meltag.com/finolex/api/';
  _headers:any;dataSet;
  constructor(public http: Http) {
    this._headers = new Headers();
    this._headers.append('finolex-user', 'finolex@meltag.io');
    this._headers.append("finolex-password", "finolex$meltag@123");
  }

  login(data,token){
    return new Promise((resolve,reject)=>{
      if(token=='undefined')
          token=0;
        this.http.get(this.url+'login?role='+data.role+'&mobile_no='+data.mobile_no+'&password='+data.password+'&device_token='+token,{headers:this._headers})
        .toPromise()
        .then(  
          res=>{
            console.log(res.json());
            resolve(res.json());
          },
          err=>{
            reject(err);
          });
    });
  }

  register(data,image){
    return new Promise((resolve,reject)=>{
      data['image']=image;
        this.http.post(this.url+'register',data,{headers:this._headers})
        .toPromise()
        .then(  
          res=>{
            console.log(res.json());
            resolve(res.json());
          },
          err=>{
            reject(err);
          });
    });
  }

  statisticsService(token,from){
    return new Promise((result,rej)=>{
      this.http.get(this.url+"dashboard?api_token="+token+"&from_date="+from,{headers:this._headers})
      .toPromise()
      .then(
        res=>{
          result(res.json());
        },
        err=>{
          rej(err);
        });
    });
  }
  getProfileData(token){
    return new Promise((result,rej)=>{
      this.http.get(this.url+"profile?api_token="+token,{headers:this._headers})
      .toPromise()
      .then(
        res=>{
          result(res.json());
        },
        err=>{
          rej(err);
        });
    });
  }
  updateImg(token,data){
    return new Promise((result,reject)=>{
      /*dataSet['api_token']=token;
      dataSet['image']=data;*/
      this.dataSet={};
      this.dataSet={
        api_token:token,
        image:data
      };
      this.http.post(this.url+"profile/image/update",this.dataSet,{headers:this._headers})
      .toPromise()
      .then(
        res=>{
          result(res.json());
        },
        err=>{
          reject(err);
        });
    });
  }
  updateProfile(data){
    return new Promise((resolve,reject)=>{
        this.http.post(this.url+"profile/update",data,{headers:this._headers})
        .toPromise()
        .then(
          res=>{
            resolve(res.json());
          },
          err=>{
              reject(err);
          });
    });
  }

  redeemCoupon(token,mobile,codes){
    return new Promise((resolve,reject)=>{
      this.http.get(this.url+"redeem/coupon-codes?api_token="+token+"&mobile_no="+mobile+"&code="+codes,{headers:this._headers})
      .toPromise()
      .then(
        res=>{
          resolve(res.json());
        },err=>{
          reject(err);
        });
    });
  }

  changePassword(data){

    return new Promise((resolve,reject)=>{
      this.http.post(this.url+"change-password",data,{headers:this._headers})
      .toPromise()
      .then(
        res=>{
          resolve(res.json());
        },
        err=>{
          reject(err);
        });
    });
  }
  changeRewardType(data){
    return new Promise((result,reject)=>{

      this.http.post(this.url+'reward-type/update',data,{headers : this._headers}).toPromise()
      .then(res=>{
        result(res.json());
      },err=>{
        reject(err);
      })

    });
  }
  getRewardTypes(token)
  {
    return new Promise((result,reject)=>{
      this.http.get(this.url+'reward-types?api_token='+ token ,{headers : this._headers}).toPromise()
      .then(res=>{
        result(res.json());
      },err=>{
          reject(err);
      });
    
    });
  }

  getBankData(token){
     return new Promise((result,reject)=>{
      this.http.get(this.url+'banks?api_token='+ token ,{headers : this._headers}).toPromise()
      .then(res=>{
        result(res.json());
      },err=>{
          reject(err);
      });
    
    });
  }

  getNotificationData(token){

      return new Promise((result,reject)=>{
          this.http.get(this.url+'notifications?api_token='+ token ,{headers : this._headers}).toPromise()
          .then(res=>{
            result(res.json());
          },err=>{
              reject(err);
          });
        
        });
  }

  sendNotificationStatus(token,id){

 // app.meltag.com/finolex/api/update/notification-status?api_token=34325&notification_id=3

    return new Promise((result,reject)=>{
          this.http.get(this.url+'update/notification-status?api_token='+ token+'&notification_id='+id ,{headers : this._headers}).toPromise()
          .then(res=>{
            result(res.json());
          },err=>{
              reject(err);
          });
        
        });
  }

  changeLanguage(data){

    return new Promise((result,reject)=>{
      this.http.post(this.url+'update/language',data,{headers : this._headers}).toPromise()
      .then(res=>{
        result(res.json());
      },err=>{
        reject(err);
      })

    });

  }

  checkVersion(){

    return new Promise((result,reject)=>{
      this.http.get(this.url+'latest-version',{headers : this._headers}).toPromise()
      .then(res=>{
        result(res.json());
      },err=>{
        reject(err);
      })

    });


  }
}
